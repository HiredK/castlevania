﻿using UnityEngine;
using System.Collections;

/**
 * @file SceneManager.cs
 * @brief Entry class that must be included in each scenes.
 * Loads the scene named "Shared".
 */

public class SceneManager : MonoBehaviour
{
	public string m_SharedSceneName = "Shared";
	private GameManager m_GameManager;
	
	/*/////////////////////////////////////////////////
	/// SERVICES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo.
	 */
	private void LoadSharedScene()
	{
		// are we are currently loading a scene?
		if (Application.isLoadingLevel) {
			return;
		}
		
		// if not, we (re)initialize the scene
		CancelInvoke("LoadSharedScene");
		Start();
	}
	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Start()
	{
		// check for the global gamemanager class
		GameObject gm = GameObject.Find("_GameManager");
		if (!gm)
		{
			// not found, we load the shared scene
			InvokeRepeating("LoadSharedScene", 0.1F, 0.1F);
			Application.LoadLevelAdditive(m_SharedSceneName);
			return;
		}
		
		// retrive the global gamemanager script
		m_GameManager = gm.GetComponent<GameManager>();
		if (m_GameManager)
		{
			// begin playing audio only when the
			// shared scene is loaded
			audio.enabled = true;
		}
	}
}