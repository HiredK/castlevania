﻿using UnityEngine;
using System.Collections;

/**
 * @file GameManager.cs
 * @brief Global class that does not get deleted
 * when changing scenes. (SingletonPattern)
 */

public class GameManager : MonoBehaviour
{
	public GameObject m_PlayerPrefab;

	private GameObject m_Player;

	/*/////////////////////////////////////////////////
	/// SERVICES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo
	 */
	private void SpawnPlayer(int _spawnID = 0)
	{
		GameObject[] list = GameObject.FindGameObjectsWithTag("SpawnPoint");
		for (uint i = 0; i < list.Length; i++)
		{
			SpawnPoint2D sp = list[i].GetComponent<SpawnPoint2D>();
			if (sp && sp.m_SpawnID == _spawnID)
			{
				m_Player.transform.position = sp.transform.position;
				return;
			}
		}
	}

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Start()
	{
		// check if a player already exists
		if ((m_Player = GameObject.FindGameObjectWithTag("Player")) == null)
		{
			// if not, instantiate a new player
			m_Player = Instantiate(m_PlayerPrefab) as GameObject;
		}

		// spawn player in the scene
		m_Player.SetActive(true);
		SpawnPlayer();
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Update()
	{
	}
}