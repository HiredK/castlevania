﻿using UnityEngine;
using System.Collections;

/**
 * @file Weapon.cs
 * @brief Base class for weapons behaviors.
 */

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class Weapon : MonoBehaviour
{
	public int m_DamageValue = 1;
	public bool m_EnableVisual = true;
	public AudioClip m_SFXHit;

	[HideInInspector]
	protected SpriteRenderer m_Renderer;
	protected BoxCollider2D m_Collider;
	protected Animator m_Animator;

	/*/////////////////////////////////////////////////
	/// ROUTNINES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo.
	 */
	public IEnumerator OnAttack(float _sec)
	{
		if (m_EnableVisual) {
			m_Animator.Play(Animator.StringToHash("sword1_attack"));
			m_Renderer.enabled = true;
		}

		m_Collider.enabled = true;

		yield return new WaitForSeconds(_sec);
		m_Renderer.enabled = false;
		m_Collider.enabled = false;
	}

	public void OnHit() {
		if (m_SFXHit) {
			audio.PlayOneShot(m_SFXHit);
		}
	}

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	void Start()
	{
		m_Renderer = GetComponent<SpriteRenderer>();
		m_Renderer.enabled = false;

		m_Collider = GetComponent<BoxCollider2D>();
		m_Collider.enabled = false;

		m_Animator = GetComponent<Animator>();
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	void Update()
	{
	}
}