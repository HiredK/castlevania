﻿using UnityEngine;
using System.Collections;

/**
 * @file CameraScreenFading.cs
 * @brief Handles screen fading by performing
 * a continuous linear interpolation between
 * m_FullScreenFadeActualAlpha and
 * m_FullScreenFadeTargetAlpha
 */

public class CameraScreenFading : MonoBehaviour
{
	public float m_FullScreenFadeActualAlpha = 0.0F;
	public float m_FullScreenFadeTargetAlpha = 0.0F;
	public float m_FullScreenFadeRate = 0.5F;
	Texture2D m_FullScreenFadeTexture = null;

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Start is called on the frame when a script
	 * is enabled just before any of the Update methods
	 * is called the first time.
	 */
	protected void Start()
	{
		if (m_FullScreenFadeTexture == null) {
			m_FullScreenFadeTexture = new Texture2D(1, 1, TextureFormat.RGB24, false);
		}
	}

	/**
	 * @brief OnGUI is called for rendering and handling
	 * GUI events.
	 */
	protected void OnGUI()
	{
		if (m_FullScreenFadeActualAlpha > 0.0F)
		{
			if (m_FullScreenFadeActualAlpha !=
			    m_FullScreenFadeTargetAlpha)
			{
				// perform linear interpolation
				m_FullScreenFadeActualAlpha = Mathf.Lerp(
					m_FullScreenFadeActualAlpha,
					m_FullScreenFadeTargetAlpha,
					Time.deltaTime * m_FullScreenFadeRate
				);
			}
			
			GUI.color = new Color(0, 0, 0, m_FullScreenFadeActualAlpha);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), m_FullScreenFadeTexture);
			GUI.color = Color.white;
		}
	}
}