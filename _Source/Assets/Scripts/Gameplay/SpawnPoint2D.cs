﻿using UnityEngine;
using System.Collections;

/**
 * @file SpawnPoint2D.cs
 * @brief Todo.
 */

public class SpawnPoint2D : MonoBehaviour
{
	public int m_SpawnID = 0;

	#if UNITY_EDITOR
	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	public virtual void OnDrawGizmos()
	{
		float factor_x = transform.localScale.x;
		float factor_y = transform.localScale.y;

		Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
		Gizmos.DrawCube(transform.position + new Vector3(0,factor_y,0), new Vector3(factor_x, factor_y * 2.0f, 0));
		Gizmos.color = Color.white;
	}
	#endif
}