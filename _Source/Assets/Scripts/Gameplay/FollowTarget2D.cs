﻿using UnityEngine;
using System.Collections;

/**
 * @file FollowTarget2D.cs
 * @brief Todo.
 */

[RequireComponent(typeof(Camera))]
public class FollowTarget2D : MonoBehaviour
{
	public GameObject m_Target;
	public Vector2 m_Bounds = new Vector2(10.0f, 10.0f);
	public float m_OffsetZ = -15.0F;
	public float m_Smooth = 2.25F;

	public bool m_FollowPlayer = true;
	public bool m_EnableSmooth = true;
	public bool m_AdjustBounds = true;
	public bool m_ShowBounds = true;

	private Camera m_MainCamera;
	private Vector3 m_Position;

	private float m_MinX;
	private float m_MaxX;
	private float m_MinY;
	private float m_MaxY;

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Start()
	{
		m_MainCamera = GetComponent<Camera>();
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Update()
	{
		if (m_FollowPlayer)
		{
			if (!m_Target) {
				m_Target = GameObject.FindGameObjectWithTag("Player");
				m_Position = m_Target.transform.position;
			}
		}

		if (m_Target) {
			Vector3 current;
			current.x = m_Target.transform.position.x;
			current.y = m_Target.transform.position.y;
			current.z = m_OffsetZ;

			if (m_EnableSmooth)
			{
				m_Position = Vector3.Lerp(m_Position, current, Time.deltaTime * m_Smooth);
				transform.position = m_Position;
			}
			else
				transform.position = current;
		}

		float vert_extent = m_MainCamera.orthographicSize;
		float horz_extent = vert_extent * Screen.width / Screen.height;
		
		// calculations assume map is position at the origin
		m_MinX = horz_extent - m_Bounds.x / 2.0f;
		m_MaxX = m_Bounds.x / 2.0f - horz_extent;
		m_MinY = vert_extent - m_Bounds.y / 2.0f;
		m_MaxY = m_Bounds.y / 2.0f - vert_extent;
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	protected void LateUpdate()
	{
		if (m_AdjustBounds) {
			Vector3 p = transform.position;
			p.x = Mathf.Clamp(p.x, m_MinX, m_MaxX);
			p.y = Mathf.Clamp(p.y, m_MinY, m_MaxY);

			transform.position = p;
		}
	}

	#if UNITY_EDITOR
	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	public virtual void OnDrawGizmos()
	{
		if (m_ShowBounds) {
			Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 0.5f);
			Gizmos.DrawCube(Vector3.zero, new Vector3(m_Bounds.x, m_Bounds.y, 0));
			Gizmos.color = Color.white;
		}
	}
	#endif
}