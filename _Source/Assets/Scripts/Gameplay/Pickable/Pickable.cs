﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * @file Pickable.cs
 * @brief Base class for pickable item behaviors.
 */

public class Pickable : MonoBehaviour
{
	static Dictionary<string, Pickable> m_Pickables = new Dictionary<string, Pickable>();
	public string m_PickableName = "Undefined";
	public int m_NumberCarrying = 0;

	private bool m_Picked = false;

	/*/////////////////////////////////////////////////
	/// SERVICES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Checks if the player is currently
	 * carrying an item
	 */
	static public bool HaveItem(string _name)
	{
		foreach(KeyValuePair<string, Pickable> entry in m_Pickables) {
			if (entry.Key == m_Pickables[_name].m_PickableName &&
			    entry.Value.m_NumberCarrying > 0)
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @brief Called by the player when the item
	 * is picked
	 */
	virtual public void OnPicked()
	{
		if (!m_Picked) {
			m_Pickables[m_PickableName].m_NumberCarrying++;
			this.gameObject.SetActive(false);
			m_Picked = true;
		}
	}

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	virtual protected void Start()
	{
		bool added = false;
		foreach(KeyValuePair<string, Pickable> entry in m_Pickables) {
			if (entry.Key == m_PickableName) {
				added = true;
			}
		}

		if (!added) {
			m_Pickables[m_PickableName] = this;
		}
	}
	
	/**
	 * @brief MonoBehaviour callback
	 */
	virtual protected void Update()
	{
	}
}