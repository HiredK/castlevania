﻿using UnityEngine;
using System.Collections;

/**
 * @file Enemy.cs
 * @brief Todo.
 */

public class Enemy : Actor
{
	public float m_MinThrowDelay = 0.5f;
	public float m_MaxThrowDelay = 2.5f;
	public int m_Health = 5;

	private bool m_IsHurting = false;

	/*/////////////////////////////////////////////////
	/// ROUTINES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo.
	 */
	IEnumerator PerformThrow(float _sec)
	{
		yield return new WaitForSeconds(_sec);
		if (m_Health > 0 && m_Throwable.m_CanThrowObject)
		{
			StartCoroutine(PerformThrow(Random.Range(m_MinThrowDelay, m_MaxThrowDelay)));
			ThrowObject(2.5f);
		}
	}

	/**
	 * @brief Todo.
	 */
	IEnumerator GetHurt(float _sec)
	{
		m_Renderer.color = Color.red;
		m_IsHurting = true;

		yield return new WaitForSeconds(_sec);

		m_Renderer.color = Color.white;
		m_IsHurting = false;
	}

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	override protected void Start()
	{
		m_Animator = GetComponent<Animator>();
		m_NormalizedHorizontalSpeed = 1;

		if (m_Throwable.m_CanThrowObject) {
			StartCoroutine(PerformThrow(Random.Range(m_MinThrowDelay, m_MaxThrowDelay)));
		}

		base.Start();

		// register events callback (delegate!)
		m_Controller.onTriggerEnterEvent += OnTriggerEnterEvent;
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	void OnTriggerEnterEvent(Collider2D _col)
	{
		if (!m_IsHurting) {
			if (_col.gameObject.tag == "Weapon")
			{
				Weapon weapon = _col.gameObject.GetComponent<Weapon>();
				if (weapon && m_Health > 0)
				{
					m_Health -= weapon.m_DamageValue;
					StartCoroutine(GetHurt(0.15f));
					weapon.OnHit();

					if (m_Health <= 0) {
						m_Animator.Play(Animator.StringToHash("death1"));
						m_NormalizedHorizontalSpeed = 0;
						m_Health = 0;
					}
				}
			}
		}
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	override protected void Update()
	{
		if (m_Health <= 0) {
			return;
		}

		Vector2 center = new Vector2(transform.position.x, transform.position.y);
		Vector2 offset = center + new Vector2(transform.localScale.x * 0.1f, 0f);
		if (!Physics2D.Raycast(offset, new Vector2(0, -1), 0.5f))
		{
			m_NormalizedHorizontalSpeed = -m_NormalizedHorizontalSpeed;
		}

		base.Update();
	}
}