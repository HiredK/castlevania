﻿using UnityEngine;
using System.Collections;

/**
 * @file Throwable.cs
 * @brief Todo.
 */

[RequireComponent(typeof(Rigidbody2D))]
public class Throwable : MonoBehaviour
{
	public float m_LifeSpan = 2.5f;
	public float m_Velocity = 0.0f;
	public float m_Angle = 80.0f;
	public float m_Force = 200f;
	
	public float m_RotationSpeed = 500f;
	public bool m_EnableRotation = true;
	
	[HideInInspector]
	protected Rigidbody2D m_RigidBody;
	
	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Start()
	{
		m_RigidBody = GetComponent<Rigidbody2D>();
		
		// create force vector and normalize it
		Vector2 force = new Vector3(m_Velocity, 0, 0);
		force.Normalize();
		
		// assign force elevation
		force.y = Mathf.Sin(m_Angle * Mathf.Deg2Rad);
		
		// apply force to object
		m_RigidBody.AddForce(force.normalized * m_Force);
		
		// destroy after delay
		Destroy(gameObject, m_LifeSpan);
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Update()
	{
		if (m_EnableRotation)
		{
			// rotate game object around z axis
			transform.eulerAngles += new Vector3(0, 0, m_RotationSpeed * Time.deltaTime);
		}
	}
}