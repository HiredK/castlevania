﻿using UnityEngine;
using System.Collections;

/**
 * @file ActorGhost.cs
 * @brief Handles a temporary sprite renderer to
 * reproduce a ghosting effect.
 */

[RequireComponent(typeof(SpriteRenderer))]
public class ActorGhost : MonoBehaviour
{
	private SpriteRenderer m_Renderer;
	
	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Start()
	{
		m_Renderer = GetComponent<SpriteRenderer>();
	}
	
	/**
	 * @brief MonoBehaviour callback
	 */
	protected void Update()
	{
		float alpha = Mathf.Lerp(m_Renderer.color.a, 0, Time.deltaTime * 10);
		m_Renderer.color = new Color
		(
			m_Renderer.color.r,
			m_Renderer.color.g,
			m_Renderer.color.b,
			alpha
		);
	}
}