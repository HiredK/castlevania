﻿using UnityEngine;
using System.Collections;

/**
 * @file Player.cs
 * @brief Todo.
 */

public class Player : Actor
{
	public bool m_EnableCloakColor = true;
	public Color m_CloakColor = Color.red;
	public float m_PulsingSpeed = 2.5f;

	public AudioClip m_SFXLand;
	public AudioClip m_SFXHurt;
	public AudioClip m_SFXPick;
	public AudioClip m_SFXAtt1;

	public Weapon m_PunchScript;
	public Weapon m_SwordScript;

	private int m_LastInputDirection = 0;
	private bool m_IsCrouching = false;
	private bool m_IsWalking = false;
	private bool m_IsFalling = false;
	private bool m_IsHurting = false;
	private bool m_IsAttacking = false;

	/*/////////////////////////////////////////////////
	/// ROUTINES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo.
	 */
	IEnumerator GetHurt(float _sec)
	{
		m_Renderer.color = Color.red;
		m_IsHurting = true;

		yield return new WaitForSeconds(_sec);

		m_Renderer.color = Color.white;
		m_IsHurting = false;
	}

	/**
	 * @brief Todo.
	 */
	IEnumerator Attack(float _sec)
	{
		m_IsAttacking = true;
		yield return new WaitForSeconds(_sec);
		m_IsAttacking = false;
	}

	/*/////////////////////////////////////////////////
	/// SERVICES:
	/////////////////////////////////////////////////*/
	/**
	 * @brief Todo.
	 */
	private void PerformHurt(float delta)
	{
		if (m_SFXHurt)
		{
			audio.Stop();
			audio.PlayOneShot(m_SFXHurt);
		}
		
		if (m_Controller.isGrounded) {
			m_Animator.Play(Animator.StringToHash("hurt1"));
			StartCoroutine(GetHurt(0.5f));
		}
		else {
			m_Animator.Play(Animator.StringToHash("hurt2"));
			StartCoroutine(GetHurt(0.5f));
		}
		
		
		// knockback the player in the opposite direction
		m_NormalizedHorizontalSpeed = (delta > 0) ? 1 : -1;
		m_InvertScaleX = false;
		
		// apply a vertical force
		m_Controller.velocity = Vector3.zero;
		m_Controller.velocity.y = CalculateJumpVerticalSpeed(0.25f);
	}

	/*/////////////////////////////////////////////////
	/// CALLBACKS:
	/////////////////////////////////////////////////*/
	/**
	 * @brief MonoBehaviour callback
	 */
	override protected void Start()
	{
		m_Animator = GetComponent<Animator>();
		base.Start();
		
		// register events callback (delegate!)
		m_Controller.onControllerCollidedEvent += OnControllerCollidedEvent;
		m_Controller.onTriggerEnterEvent += OnTriggerEnterEvent;
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	void OnControllerCollidedEvent(RaycastHit2D _hit)
	{
		m_Jumping.m_HoldingJumpButton = false;
		m_InputJump = false;
	}
	
	/**
	 * @brief MonoBehaviour callback
	 */
	void OnTriggerEnterEvent(Collider2D _col)
	{
		if (_col.gameObject.tag == "Pickable") {
			Pickable item = _col.gameObject.GetComponent<Pickable>();
			if (item)
			{
				if (m_SFXPick) {
					audio.PlayOneShot(m_SFXPick);
				}

				item.OnPicked();
			} return;
		}

		if (!m_IsHurting) {
			if (_col.gameObject.tag == "Enemy")
			{
				float delta = transform.position.x - _col.transform.position.x;
				CC2DTriggerHelper helper = _col.gameObject.GetComponent<CC2DTriggerHelper>();
				if (helper)
				{
					Enemy enemy = helper._parentCharacterController.gameObject.GetComponent<Enemy>();
					if (enemy && enemy.m_Health > 0)
					{
						PerformHurt(delta);
						return;
					}
				}
				else {
					Throwable item = _col.gameObject.GetComponent<Throwable>();
					if (item)
					{
						GameObject.Destroy(item.gameObject);
						PerformHurt(delta);
						return;
					}
				}
			}
		}
	}

	/**
	 * @brief MonoBehaviour callback
	 */
	override protected void Update()
	{
		if (m_EnableCloakColor) {
			float pulsing = Mathf.PingPong(Time.time * m_PulsingSpeed, 1.0f);
			renderer.material.SetColor("_Color2", m_CloakColor * pulsing);
		}
		else {
			renderer.material.SetColor("_Color2", Color.clear);
		}

		if (!m_IsHurting && !m_IsAttacking)
		{
			// is crouching?
			if (Input.GetAxisRaw("p1_key_AxisY") < 0) {
				if (m_Controller.isGrounded)
				{
					if (!m_IsCrouching) {
						m_Animator.Play(Animator.StringToHash("crouch1"));
						m_IsCrouching = true;
					}

					m_NormalizedHorizontalSpeed = 0;
					m_IsWalking = false;
					m_InputJump = false;
				}
			}
			else {
				if (Input.GetAxisRaw("p1_key_AxisX") > 0) {
					if (m_Controller.isGrounded)
					{
						if (m_LastInputDirection == -1) {
							m_Animator.Play(Animator.StringToHash("turn1"));
						}

						m_IsWalking = true;
					}

					m_NormalizedHorizontalSpeed =  1;
					m_LastInputDirection =  1;
				}
				else
				if (Input.GetAxisRaw("p1_key_AxisX") < 0) {
					if (m_Controller.isGrounded)
					{
						if (m_LastInputDirection ==  1) {
							m_Animator.Play(Animator.StringToHash("turn1"));
						}

						m_IsWalking = true;
					}

					m_NormalizedHorizontalSpeed = -1;
					m_LastInputDirection = -1;
				}
				else {
					if (m_Controller.isGrounded) {
						m_IsWalking = false;
					}

					m_NormalizedHorizontalSpeed =  0;
				}


				// is attacking?
				if (Input.GetButtonDown("p1_key_B1")) {
					if (m_SFXAtt1)
					{
						audio.Stop();
						audio.PlayOneShot(m_SFXAtt1);
					}

					if (m_SwordScript && Pickable.HaveItem("Sword")) {
						StartCoroutine(m_SwordScript.OnAttack(0.2f));
					}
					else
					if (m_PunchScript) {
						StartCoroutine(m_PunchScript.OnAttack(0.2f));
					}

					if (m_Controller.isGrounded)
					{
						// start grounded attack
						m_Animator.Play(Animator.StringToHash("attack1"));
						StartCoroutine(Attack(0.25f));

						// stop horizontal movement
						m_NormalizedHorizontalSpeed = 0;
						m_Controller.velocity.x = 0;
					}
					else 
					{
						// start air attack
						m_Animator.Play(Animator.StringToHash("attack2"));
						StartCoroutine(Attack(0.25f));
					}
				}

				// is jumping?
				if (Input.GetButtonDown("p1_key_B0")) {
					if (m_Controller.isGrounded)
					{
						if (m_NormalizedHorizontalSpeed == 0) {
							//m_Animator.Play(Animator.StringToHash("jump1"));
							m_Animator.Play(Animator.StringToHash("jump2"));
						}
						else {
							m_Animator.Play(Animator.StringToHash("jump2"));
						}

						m_IsWalking = false;
						m_InputJump = true;
					}
				}
				else
				if (Input.GetButtonUp("p1_key_B0")) {
					m_InputJump = false;
				}

				m_IsCrouching = false;
			}

			// just landed?
			if (m_IsFalling && m_Controller.isGrounded)
			{
				if (m_SFXLand) {
					audio.PlayOneShot(m_SFXLand);
				}

				// reset velocity
				m_Controller.velocity = Vector3.zero;
			}
			
			// is falling?
			bool falling = (!m_Controller.isGrounded && m_Velocity.y < 0);
			if (m_IsFalling != falling)
			{
				m_Animator.Play(Animator.StringToHash("fall1"));
				m_IsFalling = falling;
			}

			m_InvertScaleX = true;
		}

		m_Animator.SetBool("Crouching", m_IsCrouching);
		m_Animator.SetBool("Walking", m_IsWalking);
		m_Animator.SetBool("Grounded", m_Controller.isGrounded);
		m_Animator.SetBool("Falling", m_IsFalling);
		m_Animator.SetBool("Hurting", m_IsHurting);
		m_Animator.SetBool("Attacking", m_IsAttacking);
		base.Update();
	}
	
	/**
	 * @brief MonoBehaviour callback
	 */
	override protected void FixedUpdate()
	{
		base.FixedUpdate();
	}
}