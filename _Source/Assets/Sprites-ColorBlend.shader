Shader "Sprites/ColorBlend"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_MaskTex ("Mask Texture", 2D) = "white" {}
		_Color1 ("Tint1", Color) = (1,1,1,1)
		_Color2 ("Tint2", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color[2] : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color1;
			fixed4 _Color2;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.color[0] = IN.color * _Color1;
				OUT.color[1] = IN.color * _Color2;
				OUT.texcoord = IN.texcoord;
				
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _MaskTex;

			fixed4 frag(v2f IN) : COLOR
			{
				float4 diff = tex2D(_MainTex, IN.texcoord) * IN.color[0];
				float4 mask = tex2D(_MaskTex, IN.texcoord) * IN.color[1];
				if (mask.a)
				{
					diff = mask;
				}
				
				return diff;
			}
		ENDCG
		}
	}
}